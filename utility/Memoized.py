# Allow deep recursions for memoization:
import sys
sys.setrecursionlimit(20000)

# to be used as function decorator:
def Memoized(function):
    cache = {}

    def function_with_cache(*args):
      if args in cache:
        return cache[args]
      result = function(*args)
      cache[args] = result
      return result

    return function_with_cache
