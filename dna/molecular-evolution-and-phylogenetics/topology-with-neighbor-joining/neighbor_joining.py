import numpy
import sys

def nearest_neighbors(distance_matrix):
  q = numpy.zeros_like(distance_matrix, float)
  n = len(distance_matrix)

  row_to_row_sum = [ sum([distance_matrix[i,j] for j in range(n)]) for i in range(n) ]
  col_to_col_sum = [ sum([distance_matrix[i,j] for i in range(n)]) for j in range(n) ]

  for i in range(n):
    for j in range(n):
      q[i,j] = (n-2)*distance_matrix[i,j] - row_to_row_sum[i] - col_to_col_sum[j]
  return min([ (q[i,j],(i,j)) for i in range(n) for j in range(n) if i != j ])

def join_neighbors(distance_matrix, names, f, g):
  name_f = names[f]
  name_g = names[g]

  n = distance_matrix.shape[0]

  u_dist = [0.0]*n

  # get distance of new node u to f and g:
  u_dist[f] = 1/2. * distance_matrix[f,g]
  if n > 2:
     # when n = 2, we get 0.0/0.0 --> 0.0 (in this case)
     u_dist[f] += 1/(2*(n-2)) * ( sum([distance_matrix[f,k] - distance_matrix[g,k] for k in range(n)]) )
  u_dist[g] = distance_matrix[f,g] - u_dist[f]

  # get distance of new node u to every other node:
  for k in range(n):
    if k not in (f,g):
      u_dist[k] = 1/2. * (distance_matrix[f,k] + distance_matrix[g,k] - distance_matrix[f,g])

  u_name = '(' + names[f] + ':' + str(u_dist[f]) + ',' + names[g] + ':' + str(u_dist[g]) + ')'
  # remove f and g from names:
  names = [ names[k] for k in range(n) if k not in (f,g) ]
  # add u to names:
  names.append(u_name)

  # remove f,g from distance_matrix:
  distance_matrix_without_f_and_g = []
  for i in range(n):
    if i not in (f,g):
      row = [distance_matrix[i,j] for j in range(n) if j not in (f,g)]
      distance_matrix_without_f_and_g.append(row)

  # add u to distance_matrix:
  u_dist_without_f_and_g = [ u_dist[k] for k in range(n) if k not in (f,g) ]
  for i in range(n-2):
    distance_matrix_without_f_and_g[i].append(u_dist_without_f_and_g[i])
  distance_matrix_without_f_and_g.append(u_dist_without_f_and_g)
  distance_matrix_without_f_and_g[-1].append(0.0) # d(u,u) = 0

  return numpy.array(distance_matrix_without_f_and_g), names

def neighbor_joining(distance_matrix, names):
  while distance_matrix.shape[0] > 1:
    min_dist, (f, g) = nearest_neighbors(distance_matrix)
    print('merge', names[f], names[g])
    distance_matrix, names = join_neighbors(distance_matrix, names, f, g)
    print(distance_matrix)
    print()

  # add trailing 0.0 so that it's valid with our newick parser
  return names[0] + ':0.0'
  
def main(args):
  if len(args) == 1:
    matrix_fname = args[0]
    names = []
    distance_matrix = []
    for line in open(matrix_fname).readlines():
      vals = line.split()
      names.append(vals[0])

      distance_matrix.append([ float(v) for v in vals[1:] ])
    distance_matrix = numpy.array(distance_matrix)

    newick_tree = neighbor_joining(distance_matrix, names)
    print(newick_tree)
  else:
    print('usage: neighbor_joining <distance_matrix_file>')
    
if __name__=='__main__':
  main(sys.argv[1:])

