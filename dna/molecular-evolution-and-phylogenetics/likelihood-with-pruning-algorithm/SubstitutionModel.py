import numpy

class DNASubstitutionModel:
  # define in derived class
  def probability_of_y_given_x(self, x, y):
    pass

# todo: needs to be adapted to gapped alignment if one is provided
class DNASubstitutionModelFelsenstein81(DNASubstitutionModel):
  def __init__(self, pi_g, pi_a, pi_t):
    pi_c = 1 - pi_g - pi_a - pi_t
    assert(pi_c >= 0.0 and pi_c <= 1.0)
    self._nucleotide_to_pi = {'G':pi_g, 'A':pi_a, 'T':pi_t, 'C':pi_c}

    self._beta = 1. / (1 - pi_g**2 - pi_a**2 - pi_t**2 - pi_c**2)

  # x evolves to y
  def probability_of_y_given_x(self, x, y, time):
    val = self._nucleotide_to_pi[y] * (1 - numpy.exp(-self._beta*time))
    if x != y:
      return val
    else:
      return numpy.exp(-self._beta*time) + val

  def prior_nucleotide_probability(self, base):
    return self._nucleotide_to_pi[base]

default_model = DNASubstitutionModelFelsenstein81(pi_g=0.3, pi_a=0.2, pi_t=0.2)
