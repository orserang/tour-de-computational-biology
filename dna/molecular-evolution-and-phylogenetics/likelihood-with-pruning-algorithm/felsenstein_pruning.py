from Bio import AlignIO
from Memoized import *
from SubstitutionModel import *
import sys

class TreeNode:
  def __init__(self, name=None):
    self.children_and_branch_lengths = []

    # only leaves need be named
    self.name = name

  def add_child(self, child, child_branch_length):
    self.children_and_branch_lengths.append( (child, child_branch_length) )

  def is_leaf(self):
    return len(self.children_and_branch_lengths) == 0

  # render in newick format:
  def __str__(self):
    result = ''
    if self.name != None:
      result += self.name

    if not self.is_leaf():
      result += '('
      for i,(child,child_branch_length) in enumerate(self.children_and_branch_lengths):
        result += str(child) + ':' + str(child_branch_length)
        if i != len(self.children_and_branch_lengths)-1:
          result += ','
      result += ')'
    return result

def read_subtree_node_and_branch_length(newick_string_no_whitespace):
  # Note: even the root needs a branch length
  right_colon_index_from_right = newick_string_no_whitespace[::-1].find(':')
  right_colon_index = len(newick_string_no_whitespace)-1 - right_colon_index_from_right

  # add +1 to skip colon itself
  branch_length = float(newick_string_no_whitespace[right_colon_index+1:])
  newick_string_no_whitespace = newick_string_no_whitespace[:right_colon_index]

  if newick_string_no_whitespace.find('(') == -1:
    # only the name remains
    return TreeNode(newick_string_no_whitespace), branch_length
  else:
    # string is bracketed by '(' and ')':
    newick_string_no_whitespace = newick_string_no_whitespace[1:-1]

    # separate subtrees by ',' when not inside parentheses
    subtree_string_slices = []
    subtree_start_index = 0

    parenthesis_depth = 0
    for i,c in enumerate(newick_string_no_whitespace):
      if c == '(':
        parenthesis_depth += 1
      elif c == ')':
        parenthesis_depth -= 1
      elif c == ',':
        if parenthesis_depth == 0:
          subtree_string_slices.append( newick_string_no_whitespace[subtree_start_index:i] )
          # add +1 to skip comma itself
          subtree_start_index = i+1
    subtree_string_slices.append( newick_string_no_whitespace[subtree_start_index:] )

    internal_node = TreeNode()
    for subtree_string in subtree_string_slices:
      child_node, child_branch_length = read_subtree_node_and_branch_length(subtree_string)
      internal_node.add_child(child_node, child_branch_length)
    return internal_node, branch_length

class Phylogeny:
  NUCLEOTIDES = 'GATC'

  def __init__(self, name_to_sequence, substitution_model, newick_string_no_whitespace):
    self._name_to_sequence = name_to_sequence
    self._substitution_model = substitution_model
    self._root, garbage_length = read_subtree_node_and_branch_length(newick_string_no_whitespace)

  @Memoized
  def _prob_x_down_at_index_given_x_at_index_and_tree(self, tree_node, base_index, base_x):
    if tree_node.is_leaf():
      seq = self._name_to_sequence[tree_node.name]
      actual_base = seq[base_index]
      if base_x == actual_base:
        return 1.0
      return 0.0
  
    # not a leaf:
    prod = 1.0
    for child_Y,branch_length in tree_node.children_and_branch_lengths:
      tot = 0.0
      for base_y in self.NUCLEOTIDES:
        tot += self._substitution_model.probability_of_y_given_x(base_x, base_y, branch_length) * self._prob_x_down_at_index_given_x_at_index_and_tree(child_Y, base_index, base_y)
      prod *= tot
    return prod
  
  def _prob_data_at_index_given_tree(self, base_index):
    tot = 0.0
    for base in self.NUCLEOTIDES:
      tot += self._substitution_model.prior_nucleotide_probability(base) * self._prob_x_down_at_index_given_x_at_index_and_tree(self._root, base_index, base)
    return tot
  
  def prob_data_given_tree(self):
    # all seqs have same length (due to gaps in global alignment). use an arbitrary one to decide on length
    n = len(list(self._name_to_sequence.values())[0])
  
    prod = 1.0
    for base_index in range(n):
      prod *= self._prob_data_at_index_given_tree(base_index)
    return prod
  
def main(args):
  if len(args) == 2:
    fasta_mult_alignment_fname, newick_tree_fname = args

    # read in the multiple alignment fasta (e.g., from clustal output):
    name_to_sequence = {record.name:record.seq for record in AlignIO.read(open(fasta_mult_alignment_fname), 'fasta') }

    newick_string_no_whitespace = open(newick_tree_fname).read().replace(' ', '').replace('\n', '').replace('\r', '').replace(';','')
    phy = Phylogeny(name_to_sequence, default_model, newick_string_no_whitespace)

    print('Likelihood:', phy.prob_data_given_tree())
  else:
    print('usage: felsenstein_pruning <multiple_alignment_fasta_file> <newick_file>')
    
if __name__=='__main__':
  main(sys.argv[1:])
