from SubstitutionModel import *

# brute force for:

# clustalo_output_demo.aln:
##>D_1 Leaf 1
##CA
##>D_2 Leaf 2
##CT
##>D_3 Leaf 3
##GC

# newick_demo.txt:
##((D_1:1, D_2:2):3, D_3:0.5):0.0
#     |______|         |
#     X_2 |____________|
#            X_1 |

from itertools import product

nucleotides = 'GATC'

def prob_seq_a_evolves_to_seq_b_in_time(seq_a, seq_b, t):
  return numpy.prod([ default_model.probability_of_y_given_x(seq_a[i], seq_b[i], t) for i in range(len(seq_a))])

D_1 = 'CA'
D_2 = 'CT'
D_3 = 'GC'
n = len(D_1)

tot = 0.0
for x_1 in product(nucleotides,repeat=n):
  for x_2 in product(nucleotides,repeat=n):
    pr_d_and_x2_given_x_1 = prob_seq_a_evolves_to_seq_b_in_time(x_1, x_2, 3) * prob_seq_a_evolves_to_seq_b_in_time(x_1, D_3, 0.5) * prob_seq_a_evolves_to_seq_b_in_time(x_2, D_1, 1) * prob_seq_a_evolves_to_seq_b_in_time(x_2, D_2, 2)
    prior_on_x_1 = numpy.prod([ default_model.prior_nucleotide_probability(base) for base in x_1 ])
    tot += pr_d_and_x2_given_x_1 * prior_on_x_1

print('Likelihood:', tot)
