import sys
import numpy
import networkx as nx
import itertools
import pygraphviz
from subprocess import call

def getchar():
  print( '[press enter to continue]' )
  garbage = input()

def get_eulerian_path(mdg):
  # todo: split into connected graphs in case coverage is not enough
  if nx.is_eulerian(mdg):
    # already has a circuit
    circuit = list(nx.eulerian_circuit(mdg))
    
    # solutions are unique up to rotations; any break point in the
    # circuit will do:
    print( 'Warning: rotations of this sequence may also work' )
    return circuit
  else:
    for a in mdg.nodes():
      if len(mdg.out_edges(a)) < len(mdg.in_edges(a)):
        end_node = a
      elif len(mdg.out_edges(a)) > len(mdg.in_edges(a)):
        start_node = a

    mdg.add_edge(end_node, start_node)
    circuit = list(nx.eulerian_circuit(mdg))
    break_index = circuit.index( (end_node, start_node) )
    return circuit[break_index+1:] + circuit[:break_index]

def build_multidigraph(short_reads, k):
  result = nx.MultiDiGraph()
  for seq in short_reads:
    for i in xrange(len(seq)-k):
      k_mer = seq[i:i+k]
      next_k_mer = seq[i+1:i+k+1]
      result.add_edge(k_mer, next_k_mer, label=k_mer[1:])
  return result

def draw_multidigraph(mdg):
  A = nx.nx_agraph.to_agraph(mdg)
  A.layout(prog='dot')
  # fixme: draw on screen
  A.draw('/tmp/p.png')

def generate_string_from_path(path):
  result = path[0][0]
  for a,b in path:
    result += b[-1]
  return result

def main(argv):
  if len(argv) != 1:
    print('usage: de_bruijn_assembly <file_of_reads>')
  else:
    fname = argv[0]
    short_reads = [ line.replace('\n', '') for line in open(fname).readlines() ]
    print( short_reads )
    n=len(short_reads[0])
    mdg = build_multidigraph(short_reads, n-1)
    draw_multidigraph(mdg)
    call(['xdg-open', '/tmp/p.png'])
    
    getchar()
  
    euler_path = get_eulerian_path(mdg)
    print() 
    print( 'best' )
    print( generate_string_from_path(euler_path)  )
  
if __name__=='__main__':
  main(sys.argv[1:])
