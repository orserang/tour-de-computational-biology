import sys
import numpy

def main(argv):
  genome, k = argv
  k = int(k)
  n = len(genome)

  for i in xrange(n-k+1):
    print genome[i:i+k]

if __name__=='__main__':
  main(sys.argv[1:])
