import sys
import numpy
import networkx as nx
import itertools
import pylab as P
P.ion()

def getchar():
  print( '[press enter to continue]' )
  garbage = input()

def overlap_length(seq_a, seq_b):
  # this can likely be done more quickly using something reminscent of
  # the KMP algorithm
  best = 0
  for overlap in xrange( min(len(seq_a),len(seq_b)) ):
    if seq_a[len(seq_a)-overlap:] == seq_b[:overlap]:
      best = overlap
  return best

def build_digraph(short_reads):
  result = nx.DiGraph()
  for i in xrange(len(short_reads)):
    for j in xrange(len(short_reads)):
      seq_a = short_reads[i]
      seq_b = short_reads[j]
      overlap = overlap_length(seq_a, seq_b)
      if overlap > 0:
        result.add_edge(seq_a, seq_b, weight=overlap)

  return result

def draw_digraph(digraph):
  P.figure(1)
  pos=nx.circular_layout(digraph)
  nx.draw_networkx(digraph, pos, node_size=0, with_labels=True, with_edge_labels=True)
  edge_labels = nx.get_edge_attributes(digraph,'weight')
  print( edge_labels )
  nx.draw_networkx_edges(digraph, pos, with_labels=True)
  nx.draw_networkx_edge_labels(digraph, pos, edge_labels=edge_labels)
  P.draw()
  P.pause(1)

def path_weight(digraph, ordering):
  total_weight = 0
  for i in xrange(len(ordering)-1):
    seq_a = ordering[i]
    seq_b = ordering[i+1]
    if seq_a in digraph and seq_b in digraph[seq_a]:
      weight = digraph[seq_a][seq_b]['weight']
    else:
      weight = -numpy.inf
    total_weight += weight
  return total_weight

def generate_string_from_path(ordering):
  result = ''
  for i in xrange(len(ordering)-1):
    seq_a = ordering[i]
    seq_b = ordering[i+1]

    overlap = overlap_length(seq_a,seq_b)

    if i != len(ordering)-1:
      result += seq_a[:len(seq_a)-overlap]
    else:
      result += seq_b

  return result

# sometimes this appproach is called SCS for "sortest common
# superstring," i.e., the shortest string to contain all reads.
def solve_tsp_exact(digraph):
  nodes = digraph.nodes()

  max_path_weight = 0
  max_path = None
  for path_length in xrange(1, len(nodes)+1):
    for ordering in itertools.permutations(nodes, path_length):
      ordering_path_weight = path_weight(digraph, ordering)
      if max_path_weight < ordering_path_weight:
        max_path_weight = ordering_path_weight
        max_path = ordering
        print( ordering_path_weight, generate_string_from_path(ordering) )

  return max_path

def main(argv):
  if len(argv) != 1:
    print('usage: tsp_assembly <file_of_reads>')
  else:
    fname = argv[0]
    short_reads = [ line.replace('\n', '') for line in open(fname).readlines() ]
    print( short_reads )
    digraph = build_digraph(short_reads)
    draw_digraph(digraph)
    tsp_path = solve_tsp_exact(digraph)
  
    print() 
    print( 'best was' )
    print( generate_string_from_path(tsp_path) )
  
if __name__=='__main__':
  main(sys.argv[1:])
