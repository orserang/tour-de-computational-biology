import sys

class Multiset(dict):
  def __init__(self, items):
    for i in items:
      self.add(i)
  def add(self, item):
    if item not in self:
      self[item] = 0
    self[item] += 1
  def remove(self, item):
    if item in self:
      self[item] -= 1
    if self[item] == 0:
      del self[item]

def overlap_length(seq_a, seq_b):
  best = 0
  for overlap in xrange( min(len(seq_a),len(seq_b)) ):
    if seq_a[len(seq_a)-overlap:] == seq_b[:overlap]:
      best = overlap
  return best

def greedy_assemble(short_reads_multiset, MIN_OVERLAP_ALLOWED=4):
  assembly = short_reads_multiset.keys()[0]
  short_reads_multiset.remove(assembly)

  while len(short_reads_multiset) > 0:
    left_overlap_and_read = max([ (overlap_length(read,assembly),read) for read in short_reads_multiset ])
    right_overlap_and_read = max([ (overlap_length(assembly,read),read) for read in short_reads_multiset ])
    if left_overlap_and_read[0] > right_overlap_and_read[0]:
      overlap = left_overlap_and_read[0]

      if overlap < MIN_OVERLAP_ALLOWED:
        break
      
      short_read = left_overlap_and_read[1]
      assembly = short_read[:len(short_read)-overlap] + assembly

      short_reads_multiset.remove(short_read)
    else:
      overlap = right_overlap_and_read[0]

      if overlap < MIN_OVERLAP_ALLOWED:
        break

      short_read = right_overlap_and_read[1]
      assembly += short_read[overlap:]

      short_reads_multiset.remove(short_read)

  return assembly

def main(argv):
  if len(argv) != 1:
    print('usage: greedy_assembly <file_of_reads>')
  else:
    fname = argv[0]
    short_reads = [ line.replace('\n', '') for line in open(fname).readlines() ]
  
    print( 'ASSEMBLY' )
    print( greedy_assemble(Multiset(short_reads)) )
  
if __name__=='__main__':
  main(sys.argv[1:])
