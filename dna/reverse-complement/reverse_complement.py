import sys

base_to_complement = {'G':'C', 'A':'T', 'T':'A', 'C':'G'}

def reverse_complement(dna_seq):
  result = ''
  for base in dna_seq[::-1]:
    result += base_to_complement[base]
  return result

def main(args):
  if len(args) == 1:
    dna_seq = args[0]

    rev_comp_dna_seq = reverse_complement(dna_seq)

    print(rev_comp_dna_seq)
  else:
    print('reverse_complement <dna_seq>')

if __name__ == '__main__':
  main(sys.argv[1:])
