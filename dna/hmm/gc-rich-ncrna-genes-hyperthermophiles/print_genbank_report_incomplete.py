from Bio import SeqIO
from Bio.Alphabet import IUPAC
import sys

def print_report(genbank_fname, windows):
  # fixme: tell us about these regions of the genbank file
  pass

def main(args):
  if len(args) < 2:
    print('usage: pssm <genbank_file> <window_1_start:window_1_end> [window_2_start:window_2_end...]')
  else:
    gb_fname = args[0]
    windows = args[1:]
    windows = [ [ int(val) for val in w.split(':') ] for w in windows ]

    print_report(gb_fname, windows)

if __name__=='__main__':
  main(sys.argv[1:])
