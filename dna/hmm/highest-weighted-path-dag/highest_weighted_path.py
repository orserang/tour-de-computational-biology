import networkx as nx
import pylab as P

from Memoized import *

@Memoized
def max_path_weight_and_predecesor_ending_at(digraph, node):
  preds = list(digraph.predecessors(node))
  if len(preds) == 0:
    return (0, None)
  
  weights_and_preds = []
  for p in preds:
    weight_prefix_path,q = max_path_weight_and_predecesor_ending_at(digraph, p)

    p_to_node_edge = digraph[p][node]
    p_to_node_edge_weight = p_to_node_edge['weight']
    weights_and_preds.append( (weight_prefix_path + p_to_node_edge_weight, p) )

  return max(weights_and_preds)

def get_highest_weighted_path_and_weight(digraph):
  ((best_weight, pred), best_end_node) = max([ (max_path_weight_and_predecesor_ending_at(digraph, node), node) for node in digraph.nodes() ])
  
  reversed_path = []
  while best_end_node != None:
    reversed_path.append(best_end_node)
    (path_weight_ending_at_pred, pred) = max_path_weight_and_predecesor_ending_at(digraph, best_end_node)
    best_end_node = pred

  return reversed_path[::-1], best_weight

def load_graph_file(graph_fname):
  digraph = nx.DiGraph()
  for start_node, end_node, edge_weight in [ line.split() for line in open(graph_fname).readlines() ]:
    edge_weight = float(edge_weight)
    digraph.add_edge(start_node, end_node, weight=edge_weight)
  return digraph

def main(args):
  if len(args) == 1:
    graph_fname = args[0]
    digraph = load_graph_file(graph_fname)
    pos=nx.spring_layout(digraph)

    print(get_highest_weighted_path_and_weight(digraph))

    #nx.draw_networkx(digraph, pos=pos)
    #nx.draw_networkx_edge_labels(digraph, pos=pos)
    #P.show()

  else:
    print('usage: highest_weighted_path <graph_file>')

if __name__=='__main__':
  main(sys.argv[1:])
